public class ArrayDeletionsLab {

    static int[] deleteElementByIndex(int[] arr, int index){
       int n = arr.length-1;
        for (int i = index; i < n ; i++) {
              arr[i] = arr[i+1];
        }
        arr[n] = 0;
        n--;
        return arr;
    }

static int[] deleteElementByValue(int[] arr, int value){
    
    for (int i = 0; i < arr.length-1 ; i++) {
        if(arr[i]==value){
            value = i;
             int n = arr.length-1;
        for (int j= value; j < n ; j++) {
              arr[j] = arr[j+1];
        }

        arr[n] = 0;
        n--;
        }
        
         }
        return arr;
   }
    
    public static void main(String[] args) throws Exception {
        int arr [] = {1, 2, 3, 4, 5};
        int index = 0;
        int value = 2;

        deleteElementByIndex(arr, index);
        System.out.println("Array after deleting element at index "+index+":");
        for (int i = 0; i < arr.length-1; i++) {
            System.out.print(arr[i]+" ");
        }

        System.out.println("\n---------");
        System.out.println("Array after deleting element with value "+value+":");
        deleteElementByValue(arr, value);
        for (int i = 0; i < arr.length-2; i++) {
            System.out.print(arr[i]+ " ");
        }
        
    }
}
