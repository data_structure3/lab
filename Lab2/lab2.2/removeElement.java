public class removeElement {

    public static int removeElement(int[] nums, int val) {
        int c = 0; 

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[c] = nums[i];
                c++;
            }
        }

        return c;
    }

    public static void main(String[] args) {
        int[] nums = { 3, 2, 2, 3 };
        int val = 3;

        System.out.println("Input: nums = " + getFormattedArray(nums) + ", val = " + val);

        int c = removeElement(nums, val);
        System.out.println("Output: " + c + ", nums = " + getFormattedArray(nums, c));
    }

    public static String getFormattedArray(int[] nums) {
        StringBuilder sb = new StringBuilder();

        sb.append("[");
        for (int i = 0; i < nums.length; i++) {
            sb.append(nums[i]);

            if (i < nums.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");

        return sb.toString();
    }

    public static String getFormattedArray(int[] nums, int c) {
        StringBuilder sb = new StringBuilder();

        sb.append("[");
        for (int i = 0; i < nums.length; i++) {
            if (i < k) {
                sb.append(nums[i]);
            } else {
                sb.append("_");
            }

            if (i < nums.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");

        return sb.toString();
    }
}
