

public class ArrayManipulation {

    public static void main(String[] args) {
        int [] numbers =  {5, 8, 3, 2, 7};

        String [] names = {"Alice", "Bob", "Charlie", "David"};
        double [] values = new double[4] ;

        System.out.print("Numbers : ");
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i]+" ");
        }

        System.out.print("\nNames : ");
        for (String i : names) {
            System.out.print(i + " ");
        }

        values[0] = 1.1;
        values[1] = 1.6;
        values[2] = 1.5;
        values[3] = 1.3;

        double sum = 0.0;
       for (int i = 0; i < values.length; i++) {
            sum += values[i];
        }
        System.out.println("\nsum = "+sum);

        double max = 0.0;
       for (int i = 0; i < values.length; i++) {
            if (values[i]>max) {
                max = values[i];
            };
        }
        System.out.println("max = "+max);
        

        String []reversedNames = new String[4];
        for (int j = names.length -1; j >=0; j--) {
            reversedNames[j] = names[names.length -1 -j];  
        }
    
        System.out.print("reversedNames : ");
         for (int i = 0; i < reversedNames.length; i++) {
            System.out.print(reversedNames[i]+" ");}


            int min = 0;
            
            for ( int i = 0; i < numbers.length; i++) {
                for (int j = i + 1; j < numbers.length; j++) {
                    if (numbers[i] > numbers[j]) {
                        min = numbers[i];
                        numbers[i] = numbers[j];
                        numbers[j] = min;
                      
                        
                    }
                }
            }

            System.out.print("\nNumbers sorted: ");
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i]+" ");
        }


        



    }
}

