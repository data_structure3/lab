import java.util.Scanner;

public class booking {
    Scanner sc = new Scanner(System.in);
    user u1 = new user();
    movie m = new movie();
    cinema c = new cinema();
    Seat s = new Seat();
    Pay p = new Pay();
    member me = new member();
    private int n = 0;
    private int co = 2;

    public void Booking() {

        System.out.println("Welcome to Booking");
        u1.showeUser();

        while(checkConfirm()){
            System.out.println("------------------------------");
            System.out.println("input 1 to see Movie \ninput 2 to see Cinema");
            n = sc.nextInt();
            if (n == 1) {
                m.showeMovie();
                c.showeCinema();       
            }
            if (n == 2) {
                c.showeCinema();
                m.showeMovie(); 
            }
            s.chooseSeat();
            me.isMember();
            p.choosePay(); 
            showBooking();
            Confirm();   
        }
        
        showBooking();
        System.out.println("\n-----Finnish-----");
    }

    public void showBooking() {
        System.out.println("------------------------------");
        u1.nameUser();
        m.printMovie();
        c.printCinema();
        m.printTime();
        s.printSeat();
        me.printTotalMember();
        p.printPay();

    }

    public void Confirm() {
        System.out.println("------------------------------");
        System.out.print("input num of Confirm your tickets\n1.Confirm\n2.Cancle\n");
        co = sc.nextInt();
        if (co == 2) {
            System.out.println("Cancle");
        }
        else if (co == 1){System.out.println("Confirm"); }
    }
   
     public boolean checkConfirm() {
        if (co == 2) {
            return true;
        }
          return false;   
     }

}
